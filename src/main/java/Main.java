import com.google.gson.Gson;
import edu.cmu.ark.*;
import spark.ResponseTransformer;

import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
        QuestionAsker qa = new QuestionAsker();
        QuestionTransducer qt = new QuestionTransducer();
        InitialTransformationStep trans = new InitialTransformationStep();
//        QuestionRanker qr = null;


        qt.setAvoidPronounsAndDemonstratives(false);

        //pre-load
        AnalysisUtilities.getInstance();


        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("--debug")) {
                GlobalProperties.setDebug(true);
            } else if (args[i].equals("--verbose")) {
                qa.printVerbose = true;
            } else if (args[i].equals("--model")) { //ranking model path
                qa.modelPath = args[i + 1];
                i++;
            } else if (args[i].equals("--keep-pro")) {
                qa.dropPro = false;
            } else if (args[i].equals("--downweight-pro")) {
                qa.dropPro = false;
                qa.downweightPronouns = true;
            } else if (args[i].equals("--downweight-frequent-answers")) {
                qa.avoidFreqWords = true;
            } else if (args[i].equals("--properties")) {
                GlobalProperties.loadProperties(args[i + 1]);
            } else if (args[i].equals("--prefer-wh")) {
                qa.preferWH = true;
            } else if (args[i].equals("--just-wh")) {
                qa.justWH = true;
            } else if (args[i].equals("--full-npc")) {
                qa.doNonPronounNPC = true;
            } else if (args[i].equals("--no-npc")) {
                qa.doPronounNPC = false;
            } else if (args[i].equals("--max-length")) {
                qa.maxLength = new Integer(args[i + 1]);
                i++;
            }
        }

        qt.setAvoidPronounsAndDemonstratives(qa.dropPro);
        trans.setDoPronounNPC(qa.doPronounNPC);
        trans.setDoNonPronounNPC(qa.doNonPronounNPC);
        QuestionRanker qr;
        if (qa.modelPath != null) {
            System.err.println("Loading question ranking models from " + qa.modelPath + "...");
            qr = new QuestionRanker();
            qr.loadModel(qa.modelPath);
        } else {
            qr = null;
        }
        Gson gs = new Gson();

        get("/hello", (req, res) -> "Hello World");
//        post("/q", (req, res) -> QuestionAsker.generateQuestions(req.body(), qa, qt, trans, qr), JsonUtil.json());
        post("/q", (req, res) -> {
            res.type("application/json");
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Expose-Headers", "Content-Length,Location");
            return gs.toJson(QuestionAsker.generateQuestions(req.body(), qa, qt, trans, qr));
        });
        options("/q", (req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "POST,OPTIONS");
            res.header("Access-Control-Allow-Headers", "content-type,authorization");
            return "Options";
        });
        post("/qjson", (req, res) -> {
            res.type("application/json");
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Expose-Headers", "Content-Length,Location");
            return gs.toJson(QuestionAsker.generateQuestions(gs.fromJson(req.body(), Sentence.class).sent, qa, qt, trans, qr));
        });
        options("/qjson", (req, res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "POST,OPTIONS");
            res.header("Access-Control-Allow-Headers", "content-type,authorization");
            return "Options";
        });
        after((req, res) -> {
//            res.type("application/json");
        });
    }
}

class JsonUtil {

    public static String toJson(Object object) {

        return new Gson().toJson(object);

    }

    public static ResponseTransformer json() {

        return JsonUtil::toJson;

    }

}