// Question Generation via Overgenerating Transformations and Ranking
// Copyright (c) 2008, 2009 Carnegie Mellon University.  All Rights Reserved.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// For more information, bug reports, fixes, contact:
//    Michael Heilman
//	  Carnegie Mellon University
//	  mheilman@cmu.edu
//	  http://www.cs.cmu.edu/~mheilman



package edu.cmu.ark;

import java.io.*;
//import java.text.NumberFormat;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

//import weka.classifiers.functions.LinearRegression;

//import edu.cmu.ark.ranking.WekaLinearRegressionRanker;
import edu.stanford.nlp.trees.Tree;


/**
 * Wrapper class for outputting a (ranked) list of questions given an entire document,
 * not just a sentence.  It wraps the three stages discussed in the technical report and calls each in turn 
 * (along with parsing and other preprocessing) to produce questions.
 * 
 * This is the typical class to use for running the system via the command line. 
 * 
 * Example usage:
 * 
    java -server -Xmx800m -cp lib/weka-3-6.jar:lib/stanford-parser-2008-10-26.jar:bin:lib/jwnl.jar:lib/commons-logging.jar:lib/commons-lang-2.4.jar:lib/supersense-tagger.jar:lib/stanford-ner-2008-05-07.jar:lib/arkref.jar \
	edu/cmu/ark/QuestionAsker \
	--verbose --simplify --group \
	--model models/linear-regression-ranker-06-24-2010.ser.gz \
	--prefer-wh --max-length 30 --downweight-pro
 * 
 * @author mheilman@cs.cmu.edu
 *
 */
public class QuestionAsker {
    String buf;
    Tree parsed;
    public boolean printVerbose = false;
    public String modelPath = null;

    public boolean preferWH = false;
    public boolean doNonPronounNPC = false;
    public boolean doPronounNPC = true;
    public Integer maxLength = 1000;
    public boolean downweightPronouns = false;
    public boolean avoidFreqWords = false;
    public boolean dropPro = true;
    public boolean justWH = false;

	public QuestionAsker(){
	}
	
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	    QuestionAsker qa = new QuestionAsker();
		QuestionTransducer qt = new QuestionTransducer();
		InitialTransformationStep trans = new InitialTransformationStep();
		QuestionRanker qr = null;
		
		
		qt.setAvoidPronounsAndDemonstratives(false);
		
		//pre-load
		AnalysisUtilities.getInstance();
		
		
		
		for(int i=0;i<args.length;i++){
			if(args[i].equals("--debug")){
				GlobalProperties.setDebug(true);
			}else if(args[i].equals("--verbose")){
                qa.printVerbose = true;
			}else if(args[i].equals("--model")){ //ranking model path
                qa.modelPath = args[i+1]; 
				i++;
			}else if(args[i].equals("--keep-pro")){
                qa.dropPro = false;
			}else if(args[i].equals("--downweight-pro")){
                qa.dropPro = false;
                qa.downweightPronouns = true;
			}else if(args[i].equals("--downweight-frequent-answers")){
                qa.avoidFreqWords = true;
			}else if(args[i].equals("--properties")){  
				GlobalProperties.loadProperties(args[i+1]);
			}else if(args[i].equals("--prefer-wh")){
                qa.preferWH = true;
			}else if(args[i].equals("--just-wh")){
                qa.justWH = true;
			}else if(args[i].equals("--full-npc")){
                qa.doNonPronounNPC = true;
			}else if(args[i].equals("--no-npc")){
                qa.doPronounNPC = false;
			}else if(args[i].equals("--max-length")){
                qa.maxLength = new Integer(args[i+1]);
				i++;
			}
		}
		
		qt.setAvoidPronounsAndDemonstratives(qa.dropPro);
		trans.setDoPronounNPC(qa.doPronounNPC);
		trans.setDoNonPronounNPC(qa.doNonPronounNPC);
		
		if(qa.modelPath != null){
			System.err.println("Loading question ranking models from "+qa.modelPath+"...");
			qr = new QuestionRanker();
			qr.loadModel(qa.modelPath);
		}
		
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			if(GlobalProperties.getDebug()) System.err.println("\nInput Text:");
			String doc;
			List<Question> outputQuestionList = new ArrayList<Question>();
			
			while(true){
                outputQuestionList.clear();
				doc = "";
				qa.buf = "";

				qa.buf = br.readLine();
				if(qa.buf == null){
					break;
				}
				doc += qa.buf;

				while(br.ready()){
					qa.buf = br.readLine();
					if(qa.buf == null){
						break;
					}
					if(qa.buf.matches("^.*\\S.*$")){
						doc += qa.buf + " ";
					}else{
						doc += "\n";
					}
				}
				if(doc.length() == 0){
					break;
				}

				long startTime = System.currentTimeMillis();
				generateQuestions(doc, qa, qt, trans, qr);

				if(GlobalProperties.getDebug()) System.err.println("Seconds Elapsed Total:\t"+((System.currentTimeMillis()-startTime)/1000.0));
				//prompt for another piece of input text
				if(GlobalProperties.getDebug()) System.err.println("\nInput Text:");
			}




		}catch(Exception e){
			e.printStackTrace();
		}
	}


    public static ArrayList<QAPair> generateQuestions(String doc, QuestionAsker qa, QuestionTransducer qt, InitialTransformationStep trans, QuestionRanker qr) {
        List<String> sentences = AnalysisUtilities.getSentences(doc);

//        try {
//            makePostRequest("Barack Obama is the president of the USA.", "http://34.193.176.0:4585");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        List<Question> outputQuestionList = new ArrayList<Question>();
        //iterate over each segmented sentence and generate questions
        List<Tree> inputTrees = new ArrayList<Tree>();
        for(String sentence: sentences){
            if(GlobalProperties.getDebug()) System.err.println("Question Asker: sentence: "+sentence);

            qa.parsed = AnalysisUtilities.getInstance().parseSentence(sentence).parse;
            inputTrees.add(qa.parsed);
        }

//        if(GlobalProperties.getDebug()) System.err.println("Seconds Elapsed Parsing:\t"+((System.currentTimeMillis()-startTime)/1000.0));

        //step 1 transformations
        long startTime = System.currentTimeMillis();
        List<Question> transformationOutput = trans.transform(inputTrees);

        System.out.println(System.currentTimeMillis() - startTime);
        startTime = System.currentTimeMillis();
        //step 2 question transducer
        for(Question t: transformationOutput){
            if(GlobalProperties.getDebug()) System.err.println("Stage 2 Input: "+t.getIntermediateTree().yield().toString());
            qt.generateQuestionsFromParse(t);
            outputQuestionList.addAll(qt.getQuestions());
        }

        System.out.println(System.currentTimeMillis() - startTime);
        startTime = System.currentTimeMillis();
        //remove duplicates
        QuestionTransducer.removeDuplicateQuestions(outputQuestionList);
        System.out.println(System.currentTimeMillis() - startTime);

        //step 3 ranking
        if(qr != null){
            qr.scoreGivenQuestions(outputQuestionList);
            boolean doStemming = true;
            QuestionRanker.adjustScores(outputQuestionList, inputTrees, qa.avoidFreqWords, qa.preferWH, qa.downweightPronouns, doStemming);
            QuestionRanker.sortQuestions(outputQuestionList, false);
        }

        //now print the questions
        //double featureValue;
        ArrayList<QAPair> result = new ArrayList<>();
        String short_ans;
        for(Question question: outputQuestionList){
            if(question.getTree().getLeaves().size() > qa.maxLength){
                continue;
            }
            if(qa.justWH && question.getFeatureValue("whQuestion") != 1.0){
                continue;
            }
            System.out.print(question.yield());
            if(qa.printVerbose) System.out.print("\t"+AnalysisUtilities.getCleanedUpYield(question.getSourceTree()));
            Tree ansTree = question.getAnswerPhraseTree();
            if(qa.printVerbose) System.out.print("\t");
            if(ansTree != null){
                short_ans = AnalysisUtilities.getCleanedUpYield(question.getAnswerPhraseTree());
                if(qa.printVerbose) System.out.print(AnalysisUtilities.getCleanedUpYield(question.getAnswerPhraseTree()));
            } else {
                short_ans = "";
            }
            if(qa.printVerbose) System.out.print("\t"+question.getScore());
            //System.err.println("Answer depth: "+question.getFeatureValue("answerDepth"));
            System.out.println();
            result.add(new QAPair(AnalysisUtilities.getCleanedUpYield(question.getSourceTree()), short_ans, question.yield(), String.valueOf(question.getScore())));
//            break;
        }
        return result;
    }

    public static void printFeatureNames(){
		List<String> featureNames = Question.getFeatureNames();
		for(int i=0;i<featureNames.size();i++){
			if(i>0){
				System.out.print("\n");
			}
			System.out.print(featureNames.get(i));
		}
		System.out.println();
	}

// PYTHON CODE BELOW
//	ArrayList<String> ffs(){
//        ArrayList<String> result = new ArrayList<>();
//        r = requests.post("http://34.193.176.0:4585", data=sent)
//
//        try:
//        triples = r.headers["triples"].split("?")[1:-2]
//        except KeyError:
//        print("No triples received.")
//        continue
//
//        for triple in triples:
//        triple = triple.replace("(","")
//        triple = triple.replace(")","")
//        triple = triple.replace("[","")
//        triple = triple.replace("]","")
//        print(triple)
//
//        if len(triple) < 2:
//        continue
//
//        try:
//        conf = float(triple[:4])
//        except:
//        continue
//
//        if conf > 0.3:
//            snt = triple[5:].split("; ")
//            if ":" in snt[0]:
//                snt[0] = snt[0].split(":")[-1]
//
//            modifiers = []
//            st = []
//            for s in snt:
//                if ":" in s:
//                    modifiers.append(s.split(":")[1])
//                else:
//                    st.append(s)
//            res = " ".join(st)
//            result.append(res+". ")
//            for mod in modifiers:
//                result.append(res + " " + mod+". ")
//
//
//    }

    static void makePostRequest(String rawData, String url) throws IOException {

        String type = "application/x-www-form-urlencoded";
        String encodedData =  URLEncoder.encode( rawData, "UTF-8" );
        URL u = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty( "Content-Type", type );
        conn.setRequestProperty( "Content-Length", String.valueOf(encodedData.length()));
        OutputStream os = conn.getOutputStream();
        os.write(encodedData.getBytes());

        Map<String, List<String>> map = conn.getHeaderFields();


        StringBuilder content;

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {

            String line;
            content = new StringBuilder();

            while ((line = in.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        }
        System.out.println();


    }
}
