package edu.cmu.ark;

public class QAPair {
    String source;
    String short_ans;
    String question;
    String score;

    public QAPair(String source, String short_ans, String question, String score) {
        this.source = source;
        this.short_ans = short_ans;
        this.question = question;
        this.score = score;
    }
}
