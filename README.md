# Question generation
This question generator is based on "Question Generation via Overgenerating Transformations and Ranking" by Michael 
Heilman and Noah Smith. The code is an extension of their code. Only `src.main.java` package is solely our work, with
modifications made also to `edu.cmu.ark.QuestionAsker`. The application includes Java Spark server accessible through 
REST API.


## Setup
1. Clone this repository
2. Download the original code from [here](http://www.cs.cmu.edu/~ark/mheilman/questions/QuestionGeneration-10-01-12.tar.gz) and copy
the folders `config`, `dict` and `lib` (not included here to reduce size) to the root directory of this application
3. Download dependencies
4. Build a JAR file with `src.main.java.Main` as the target
5. Copy the JAR to the root directory of this application
6. Run the JAR

## Docker
The application has Dockerfile included. To create an image and run a container, follow these steps:
1. Run `docker build -t <image_name> .` (Replace <image_name> with name of your choosing)
2. Run `docker run -d -p <port>:4567 <image_name>` (Choose a port for the application)


## Usage
The application runs on port 4567 by default. There are two endpoints:
* `/q` - accepts the sentence from which the questions should be generated in plain texts
* `/qjson` - accepts JSON dictionary object with single element: the key is "sent" and value is the sentence from which the questions
should be generated
Both endpoints return JSON.
