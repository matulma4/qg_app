from java:8
RUN mkdir /QG_App
ADD . /QG_App
WORKDIR /QG_App

# RUN mv /etc/alternatives/java /etc/alternatives/java8
# RUN apt-get update -y && apt-get install maven -y

# Restore Java 8
# RUN mv -f /etc/alternatives/java8 /etc/alternatives/java
# RUN ls -l /usr/bin/java && java -version

# RUN mvn install
# RUN jar cvf app.jar -C /QG_App .
CMD ["java", "-Xmx1200m", "-jar", "QG_App.jar", "--verbose", "--model", "models/linear-regression-ranker-reg500.ser.gz", "--prefer-wh", "--max-length", "12", "--downweight-pro"]
EXPOSE 4567
